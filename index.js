var express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const routes = require('./src/controllers/crmContoller');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.connect('mongodb://localhost/test', {
  useNewUrlParser: true
});

//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: F I R S T   E X A M P L E   F O R   C R E A T E   T A B L E   O N   M O N G O D B : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//


//
// ─── START ──────────────────────────────────────────────────────────────────────
//
const Cat = mongoose.model('Cat', {name: String});

const kitty = new Cat({name: 'mimi'});

kitty.save().then((res) => {
  console.log(res);
  console.log('Meow');
})
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

//
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: S E C O N D   E X A M P L E   F O R   C R E A T E   T A B L E   A N D   M E T H O D   P O S T   O N   M O N G O D B : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

//
// ─── START ──────────────────────────────────────────────────────────────────────
//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const BlogSchema = require('./src/models/crmModel');
const blogModel = mongoose.model('blog', BlogSchema);

app.post('/newBlog', (req, res) => {
  let blog = new blogModel(req.body);
  blog.save((err, blogModel) => {
    if(err){
      res.send(err);
    }
    res.json(blog);
  })
});
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────── III ──────────
//   :::::: T H I R D   F O R   G E T   M E T H O D   F R O M   T A B L E   B L O G S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

//
// ─── START ──────────────────────────────────────────────────────────────────────
//
let getAllBlogs = (req, res) => {
  blogModel.find({}, (err, blogs) => {
    if(err){
      res.send(err);
    }else {
      res.send(blogs);
    }
  })
}

app.get('/getBlogs', getAllBlogs);
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

//
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── IV ──────────
//   :::::: F O U R T H   F O R   G E T   M E T H O D   F R O M   T A B L E   B L O G S   F O R   E A C H   I D : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//


//
// ─── START ──────────────────────────────────────────────────────────────────────
//

let getBlogByID = (req, res) => {
  blogModel.findById((req.params.blogId), (err, blog) => {
    if(err){
      res.send(err);
    }else {
      res.send(blog);
    }
  })
}

app.get('/blog/:blogId', getBlogByID);
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── V ──────────
//   :::::: F I F T H   F O R   P U T   M E T H O D   F R O M   T A B L E   B L O G S   F O R   E A C H   I D : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

//
// ─── START ──────────────────────────────────────────────────────────────────────
//

let putBlogByID = (req, res) => {
  blogModel.findOneAndUpdate(
    {_id: req.params.blogId}, 
    req.body, 
    {new: true},
    (err, updateBlog) => {
    if(err){
      res.send(err);
    }else {
      res.send(updateBlog);
    }
  })
}

app.put('/blog/:blogId', putBlogByID);
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

//
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── VI ──────────
//   :::::: S I X T H   F O R   D E L E T E   M E T H O D   F R O M   T A B L E   B L O G S   F O R   E A C H   I D : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//


//
// ─── START ──────────────────────────────────────────────────────────────────────
//

let deleteBlogByID = (req, res) => {
  blogModel.remove(
    {_id: req.params.blogId},
    (err, blog) => {
    if(err){
      res.send(err);
    }else {
      res.send({message: 'Blog deleted successfully'});
    }
  })
}

app.delete('/blog/:blogId', deleteBlogByID);
//
// ─── END ────────────────────────────────────────────────────────────────────────
//

// app.use(function(req, res, next) {
//   console.log('Time', Date.now());
// })

//
// ──────────────────────────────────────────────────────────────────────────────── VII ──────────
//   :::::: S E R V I N G   S T A T I C   F I L E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────
//

//
// ─── START ──────────────────────────────────────────────────────────────────────
//

app.use(express.static('public/assets/images'));


app.get('/', function(req, res, next){
  console.log('Req Method: ', req.method)
  next();
}, function(req, res, next){
  console.log('Request Original Url', req.originalUrl);
  next();
}, function(req, res, next){
  res.send('Request was Successful');
})

app.listen(PORT, () => {
  console.log(`Server is running on PORT: ${PORT}`)
});